package br.com.inter.desafio.controllers.desafios;

import br.com.inter.desafio.dto.DesafioDTO;
import br.com.inter.desafio.dto.EntidadeDTO;
import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.exception.ValorMaxException;
import br.com.inter.desafio.exception.ValorMinException;
import br.com.inter.desafio.models.Entidade;
import io.swagger.annotations.*;

import java.util.List;

@Api("Desafio")
public interface DesafiosControllerDocs {

    @ApiOperation(value = "Calcula o digito unico e retorna o endereço encontrado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o desafio"),
            @ApiResponse(code = 400, message = "Numeros ou cep digitado não está de acordo com o esperado"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    EntidadeDTO postDesafio(@ApiParam(name = "numero", type = "String", value = "Numero a calcular", example = "9875",required = true) String numero,
                            @ApiParam(name = "vezesConcatenada", type = "Integer", value = "Vezes que o numero Inserido será calculado", example = "4", required = true) Integer vezesConcatenada,
                            @ApiParam(name = "cep", type = "String", value = "Cep Para Encontrar o Endereço", example = "30190-924", required = true) String cep) throws ValorMaxException, FormatoDeCepErradoException, ValorMinException, CepNaoEncontrado;

    @ApiOperation(value = "Lista todos os desafios.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista de todos desafios resolvidos"),
    })
    List<EntidadeDTO> listAll();

    @ApiOperation(value = "Apaga todos os desafios realizados")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Historico limpado com sucesso"),
            @ApiResponse(code = 500, message = "Deu algum erro!")
    })
    void deleteAll();
}
