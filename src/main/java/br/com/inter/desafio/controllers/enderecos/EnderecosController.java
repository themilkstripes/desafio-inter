package br.com.inter.desafio.controllers.enderecos;

import br.com.inter.desafio.dto.EnderecoDTO;
import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.models.Endereco;
import br.com.inter.desafio.services.EnderecoService;
import br.com.inter.desafio.utils.JasonUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class EnderecosController implements EnderecosControllerDocs{

    private EnderecoService service;

    private JasonUtils strToJson;

    @Override
    @PostMapping(value = "/endereco", produces = "application/json")
    public EnderecoDTO postEndereco(@RequestParam String cep) throws FormatoDeCepErradoException, CepNaoEncontrado {

        return service.postEndereco(cep);
    }

    @Override
    @GetMapping(value = "/enderecos", produces = "application/json")
    public List<EnderecoDTO> listAll() {

        return service.listAll();
    }

    @Override
    @DeleteMapping("/enderecos/limpar")
    public void deleteAll() {

        service.deleteAll();
    }
}
