package br.com.inter.desafio.controllers.enderecos;

import br.com.inter.desafio.dto.EnderecoDTO;
import br.com.inter.desafio.dto.EntidadeDTO;
import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.models.Endereco;
import io.swagger.annotations.*;

import java.util.List;

@Api("Endereco")
public interface EnderecosControllerDocs {

    @ApiOperation(value = "Traz o endereço a partir de um cep dado")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Retorna o endereço"),
            @ApiResponse(code = 400, message = "Cep está digitado errado ou em um formato não aceito"),
            @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
    })
    EnderecoDTO postEndereco(
            @ApiParam(name = "cep", type = "String", value = "Cep para encontrar o endereço", example = "30190-924",required = true) String cep)
            throws FormatoDeCepErradoException, CepNaoEncontrado;

    @ApiOperation(value = "Lista todos os endereços consultados.")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Lista de todos endereços"),
    })
    List<EnderecoDTO> listAll();

    @ApiOperation(value = "Apaga todos os endereços consultados")
    @ApiResponses(value = {
            @ApiResponse(code = 204, message = "Historico limpado com sucesso"),
            @ApiResponse(code = 500, message = "Deu algum erro!")
    })
    void deleteAll();
}
