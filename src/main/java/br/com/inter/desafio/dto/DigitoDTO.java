package br.com.inter.desafio.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class DigitoDTO {

    @ApiModelProperty(value = "Data Transfer Object - Digito Unico: Id")
    private Long id;

    @ApiModelProperty(value = "Data Transfer Object - Digito Unico: Numero usado")
    @NotNull
    private String numero;

    @ApiModelProperty(value = "Data Transfer Object - Digito Unico: Digito unico encontrado")
    @NotNull
    private Integer digitoUnico;

    public DigitoDTO(String numero, Integer digitoUnico) {
        this.numero = numero;
        this.digitoUnico = digitoUnico;
    }
}
