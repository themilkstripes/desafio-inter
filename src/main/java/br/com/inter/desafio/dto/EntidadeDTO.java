package br.com.inter.desafio.dto;

import br.com.inter.desafio.models.Desafio;
import br.com.inter.desafio.models.Entidade;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class EntidadeDTO {

    @ApiModelProperty(value = "Data Transfer Object - Entidade: Id")
    private Long id;

    @ApiModelProperty(value = "Data Transfer Object - Entidade: Desafios Resolvidos")
    @NotNull
    private Desafio desafio;

    public EntidadeDTO(Desafio desafio) {
        this.desafio = desafio;
    }
}
