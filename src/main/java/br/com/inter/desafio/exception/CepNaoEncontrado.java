package br.com.inter.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class CepNaoEncontrado extends Exception {

    public CepNaoEncontrado(String cep) {
        super(String.format("Cep %s não foi encontrado", cep));
    }
}
