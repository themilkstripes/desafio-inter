package br.com.inter.desafio.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class ValorMinException extends Exception {

    public ValorMinException(Integer num) {
        super(String.format("Alto lá! Abaixo do permitido. Você inseriu %d!!", num));
    }
}
