package br.com.inter.desafio.mapper;

import br.com.inter.desafio.dto.DigitoDTO;
import br.com.inter.desafio.models.Digito;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper
public interface DigitoMapper {

    DigitoMapper INSTANCE = Mappers.getMapper(DigitoMapper.class);

    Digito toModel(DigitoDTO digitoDTO);

    DigitoDTO toDTO(Digito digito);
}
