package br.com.inter.desafio.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.FetchType;
import javax.persistence.CascadeType;

@Data
@Entity
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Desafio {

    @ApiModelProperty(value = "Codigo do desafio")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "Numero usado para calcular e o resultado")
    @OneToOne(cascade = CascadeType.ALL)
    private Digito digitoUnico;

    @ApiModelProperty(value = "Endereço encontrado a partir do cep dado")
    @OneToOne(cascade = CascadeType.ALL)
    private Endereco endereco;

    public Desafio(Digito digitoUnico, Endereco endereco) {
        this.digitoUnico = digitoUnico;
        this.endereco = endereco;
    }
}
