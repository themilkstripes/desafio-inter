package br.com.inter.desafio.models;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;

@Data
@Entity
@EqualsAndHashCode
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Digito {

    @ApiModelProperty(value = "Codigo do digito unico calculado")
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ApiModelProperty(value = "Numero usado para calculo")
    @Column
    private Integer numero;

    @ApiModelProperty(value = "Digito unico encontrado")
    @Column
    private Integer digitoUnico;

    public Digito(Integer numero, Integer digitoUnico) {
        this.numero = numero;
        this.digitoUnico = digitoUnico;
    }
}
