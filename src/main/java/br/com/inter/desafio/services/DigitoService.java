package br.com.inter.desafio.services;

import br.com.inter.desafio.exception.ValorMaxException;
import br.com.inter.desafio.exception.ValorMinException;
import br.com.inter.desafio.mapper.DigitoMapper;
import br.com.inter.desafio.models.Digito;
import br.com.inter.desafio.repository.DigitoRepository;
import br.com.inter.desafio.utils.DigitoUnicoUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DigitoService {

    private final DigitoMapper digitoMapper = DigitoMapper.INSTANCE;

    private DigitoRepository digitoRepository;

    private DigitoUnicoUtils digitoUnicoUtils;

    public Digito digitoUnico(String numero, Integer vezesConcatenadas) throws ValorMaxException, ValorMinException {

        validarNumeroInserido(numero);
        validarVezesConcatenadasInserida(vezesConcatenadas);

        String numeroParaCalculo = digitoUnicoUtils.vezesParaConcatenar(Integer.parseInt(numero), vezesConcatenadas);
        Integer unicoDigito = digitoUnicoUtils.digitoUnico(numeroParaCalculo);

        Digito digito = new Digito(Integer.parseInt(numero), unicoDigito);
        Digito digitoSalvo = digitoRepository.save(digito);

        return digitoSalvo;
    }

    public List<Digito> listAll() {

        return digitoRepository.findAll()
                .stream()
                .collect(Collectors.toList());
    }

    private void validarNumeroInserido(String numero) throws ValorMaxException, ValorMinException {
        int num = Integer.parseInt(numero);

        if (num < 1) {
            throw new ValorMinException(num);
        } else if(num >= Integer.MAX_VALUE) { // 2147483647
            throw new ValorMaxException(num);
        }
    }

    public void deleteAll()  {
        digitoRepository.deleteAll();
    }

    private void validarVezesConcatenadasInserida(Integer num) throws ValorMaxException, ValorMinException {
        if (num < 1) {
            throw new ValorMinException(num);
        } else if(num >= 100000) {
            throw new ValorMaxException(num);
        }
    }
}
