package br.com.inter.desafio.services;

import br.com.inter.desafio.dto.EnderecoDTO;
import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.mapper.EnderecoMapper;
import br.com.inter.desafio.models.Endereco;
import br.com.inter.desafio.repository.EnderecoRepository;
import br.com.inter.desafio.utils.EnderecoUtils;
import br.com.inter.desafio.utils.JasonUtils;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor(onConstructor = @__(@Autowired))
public class EnderecoService {

    private final String URL_VIACEP = "https://viacep.com.br/ws/";

    private final EnderecoMapper enderecoMapper = EnderecoMapper.INSTANCE;

    private EnderecoRepository enderecoRepository;

    private EnderecoUtils enderecoUtils;

    private JasonUtils strToJson;

    public EnderecoDTO postEndereco(String cep) throws FormatoDeCepErradoException, CepNaoEncontrado {

        String e = enderecoUtils.getCep(cep);
        Endereco end = strToJson.stringToJson(e);

        Endereco enderecoSalvo = enderecoRepository.save(end);

        return enderecoMapper.toDTO(enderecoSalvo);
    }

    public List<EnderecoDTO> listAll() {

        return enderecoRepository.findAll()
                .stream()
                .map(enderecoMapper::toDTO)
                .collect(Collectors.toList());
    }

    public void deleteAll()  {
        enderecoRepository.deleteAll();
    }
}
