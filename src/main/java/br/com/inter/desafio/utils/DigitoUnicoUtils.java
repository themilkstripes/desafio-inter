package br.com.inter.desafio.utils;

import org.springframework.stereotype.Component;

@Component
public class DigitoUnicoUtils {

    public Integer digitoUnico(String numero){
        Integer somaNumeros = 0;

        if(numero.length() == 1){
            return Integer.parseInt(numero);
        }

        for (char num: numero.toCharArray()) {
            somaNumeros += Integer.parseInt(Character.toString(num));
        }

        if(somaNumeros.toString().length() > 1){
            return digitoUnico(somaNumeros.toString());
        }

        return somaNumeros;
    }

    public String vezesParaConcatenar(Integer numero, Integer multiplicador){
        StringBuilder numeroMultiplicado = new StringBuilder();

        for(int i = 0; i < multiplicador; i++){
            numeroMultiplicado.append(numero);
        }
        return numeroMultiplicado.toString();
    }
}