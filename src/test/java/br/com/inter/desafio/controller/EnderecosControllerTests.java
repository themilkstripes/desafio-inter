package br.com.inter.desafio.controller;

import br.com.inter.desafio.builder.EnderecoDTOBuilder;
import br.com.inter.desafio.controllers.enderecos.EnderecosController;
import br.com.inter.desafio.dto.DigitoDTO;
import br.com.inter.desafio.dto.EnderecoDTO;
import br.com.inter.desafio.exception.CepNaoEncontrado;
import br.com.inter.desafio.exception.FormatoDeCepErradoException;
import br.com.inter.desafio.exception.ValorMaxException;
import br.com.inter.desafio.mapper.EnderecoMapper;
import br.com.inter.desafio.models.Digito;
import br.com.inter.desafio.models.Endereco;
import br.com.inter.desafio.services.EnderecoService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.view.json.MappingJackson2JsonView;

import java.util.Collections;

import static br.com.inter.desafio.utils.JsonConvertionUtils.asJsonString;
import static org.hamcrest.core.Is.is;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
public class EnderecosControllerTests {

    private static final String URL_POST = "/api/endereco";
    private static final String URL_GET = "/api/enderecos";

    private MockMvc mockMvc;

    @Mock
    private EnderecoService enderecoService;

    @InjectMocks
    private EnderecosController enderecosController;

    private EnderecoMapper mapper = EnderecoMapper.INSTANCE;

    @BeforeEach
    void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(enderecosController)
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .setViewResolvers((viewName, locale) -> new MappingJackson2JsonView())
                .build();
    }

    @Test
    void quandoPOST_AFuncaoParaAnalizarOCepFoiChamada_EntaoRetornarOKStatusEOEndereco() throws Exception {
        // given
        EnderecoDTO enderecoDTO = EnderecoDTOBuilder.builder().build().toEnderecoDTO();

        // when
        when(enderecoService.postEndereco("08567040")).thenReturn(enderecoDTO);

        // then
        mockMvc.perform(post(URL_POST).param("cep","08567040")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(enderecoDTO)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.cep", is(enderecoDTO.getCep())))
                .andExpect(jsonPath("$.logradouro", is(enderecoDTO.getLogradouro())))
                .andExpect(jsonPath("$.bairro", is(enderecoDTO.getBairro())))
                .andExpect(jsonPath("$.localidade", is(enderecoDTO.getLocalidade())))
                .andExpect(jsonPath("$.uf", is(enderecoDTO.getUf())));
    }

    @Test
    void quandoGET_listaComEnderecoEChamada_EntaoRentornaOKStatus() throws Exception {
        // given
        EnderecoDTO enderecoDTO = EnderecoDTOBuilder.builder().build().toEnderecoDTO();

        // when
        when(enderecoService.listAll()).thenReturn(Collections.singletonList(enderecoDTO));

        // then
        mockMvc.perform(get(URL_GET)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].cep", is(enderecoDTO.getCep())))
                .andExpect(jsonPath("$[0].logradouro", is(enderecoDTO.getLogradouro())))
                .andExpect(jsonPath("$[0].bairro", is(enderecoDTO.getBairro())))
                .andExpect(jsonPath("$[0].localidade", is(enderecoDTO.getLocalidade())))
                .andExpect(jsonPath("$[0].uf", is(enderecoDTO.getUf())));
    }

    @Test
    void quandoGET_listaComEnderecoVaziaEChamada_EntaoRentornaOKStatus() throws Exception {
        // when
        when(enderecoService.listAll()).thenReturn(Collections.EMPTY_LIST);

        //then
        mockMvc.perform(get(URL_GET)
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

    }

    @Test
    void dadoUmCepInesperado_QuandoProcurarPeloEndereco_RetornarStatusBadRequestELancarAExcecao() throws Exception {
        // given
        Endereco endereco = new Endereco("08567-040", "Rua Maria Sanches Gambogi", "Jardim Madre Ângela", "", "Poá", "SP");
        EnderecoDTO dto = mapper.toDTO(endereco);

        // when
        when(enderecoService.postEndereco("085670933-2222")).thenThrow(FormatoDeCepErradoException.class);

        // then
        mockMvc.perform(post(URL_POST).param("cep","085670933-2222")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto))).andExpect(status().isBadRequest());
    }

    @Test
    void dadoUmCepIncorreto_QuandoProcurarPeloEndereco_RetornarStatusNotFoundELancarExcecao() throws Exception {
        // given
        Endereco endereco = new Endereco("08567-040", "Rua Maria Sanches Gambogi", "Jardim Madre Ângela", "", "Poá", "SP");
        EnderecoDTO dto = mapper.toDTO(endereco);

        // when
        when(enderecoService.postEndereco("01010-09")).thenThrow(CepNaoEncontrado.class);

        // then
        mockMvc.perform(post(URL_POST).param("cep","01010-09")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(dto))).andExpect(status().isNotFound());
    }

    @Test
    void quandoOUsuarioQuiserLimparOHistoricoDeEnderecosEncontrados_RetornarOkStatus() throws Exception {

        // when
        doNothing().when(enderecoService).deleteAll();

        //then
        mockMvc.perform(delete(URL_GET + "/limpar")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());

        verify(enderecoService, times(1)).deleteAll();
    }

}
