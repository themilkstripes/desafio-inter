package br.com.inter.desafio.utils;

import org.junit.jupiter.api.BeforeEach;
import org.mockito.junit.jupiter.MockitoExtension;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;

import static org.assertj.core.api.Assertions.assertThat;


@ExtendWith(MockitoExtension.class)
public class DigitoUnicoUtilsTests {

    @Mock
    private DigitoUnicoUtils digitoUnicoUtils;

    @BeforeEach
    void init(){
        digitoUnicoUtils = new DigitoUnicoUtils();
    }


    @Test
    public void dadoUmNumero_QuandoAFuncaoDigitoUnicoForChamada_ResultarNoValorCorreto() {
        // given
        Integer numero = 9875;

        // when
        Integer resultado = digitoUnicoUtils.digitoUnico(numero.toString());

        // then
        assertThat(resultado).isEqualTo(2);
    }

    @Test
    public void dadoUmNumeroEAsVezesQueSeraConcatenada_QuandoAFuncaoVezesConcatenadaForChamada_ResultarNaStringCorreta() {
        // given
        Integer numero = 9875;
        Integer vezesParaConcatenar = 4;

        // when
        String resultado = digitoUnicoUtils.vezesParaConcatenar(numero, vezesParaConcatenar);

        // then
        assertThat(resultado).isEqualTo("9875987598759875");
    }

    @Test
    public void dadoUmNumeroEAsVezesQueSeraConcatenado_ChamarAFuncaoDeConcatenarECalcularODigitoUnico_RetornarODigitoUnico() {
        // given
        Integer numero = 9875;
        Integer vezesParaConcatenar = 4;

        // when
        String numeroParaCalculo = digitoUnicoUtils.vezesParaConcatenar(numero, vezesParaConcatenar);
        Integer novoDigitoUnico = digitoUnicoUtils.digitoUnico(numeroParaCalculo);

        // then
        assertThat(novoDigitoUnico).isEqualTo(8);
    }
}
